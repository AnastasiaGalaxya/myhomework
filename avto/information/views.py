from django.shortcuts import render
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import *
from .models import *
from rest_framework import viewsets
from django.shortcuts import get_object_or_404

# Create your views here.


class AvtomobCreateView(generics.CreateAPIView):
    serializer_class = AvtomobDetailSerializer
class NationCreateView(generics.CreateAPIView):
    serializer_class = NationDetailSerializer
class ZapchastiCreateView(generics.CreateAPIView):
    serializer_class = ZapchastiDetailSerializer

class AvtomobListView(generics.ListAPIView):
    serializer_class = AvtomobDetailSerializer
    queryset= Avtomob.objects.all()
class AvtomobDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AvtomobDetailSerializer
    queryset = Avtomob.objects.all()

class NationListView(generics.ListAPIView):
    serializer_class = NationDetailSerializer
    queryset= Nation.objects.all()
class NationDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = NationDetailSerializer
    queryset = Nation.objects.all()

class ZapchastiListView(generics.ListAPIView):
    serializer_class = ZapchastiDetailSerializer
    queryset= Zapchasti.objects.all()
class ZapchastiDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ZapchastiDetailSerializer
    queryset = Zapchasti.objects.all()
