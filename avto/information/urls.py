from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'informaton'
urlpatterns = [
    path('avtomob/create/', AvtomobCreateView.as_view()),
    path('zapchasti/create/', ZapchastiCreateView.as_view()),
    path('nation/create/', NationCreateView.as_view()),
    path('avtomoblist/', AvtomobListView.as_view()),
    path('avtomobedit/<int:pk>/', AvtomobDetailView.as_view()),
    path('nationlist/', NationListView.as_view()),
    path('nationedit/<int:pk>/', NationDetailView.as_view()),
    path('zapchastilist/', ZapchastiListView.as_view()),
    path('zapchastiedit/<int:pk>/', ZapchastiDetailView.as_view())

]

