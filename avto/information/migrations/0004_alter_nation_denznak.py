# Generated by Django 3.2.14 on 2022-07-11 19:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('information', '0003_nation_denznak'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nation',
            name='denznak',
            field=models.CharField(max_length=100, verbose_name='Валюта'),
        ),
    ]
