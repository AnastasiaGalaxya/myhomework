from django.db import models

# Create your models here.

class Avtomob(models.Model):
    name_model = models.CharField(
        verbose_name='Модель',
        max_length=100
        )
    nation = models.ForeignKey('Nation', on_delete=models.CASCADE)
    def __str__(self):
        return self.name_model

    number = models.IntegerField()

class Nation(models.Model):
    name = models.CharField(
        verbose_name='Название',
        max_length=100
        )
    denznak = models.CharField(
        verbose_name='Валюта',
        max_length=100
    )
    is_Eurosoys = models.BooleanField(default=False)
    def __str__(self):
        return self.name

class Zapchasti(models.Model):
    vid = models.CharField(
        verbose_name='Наименование',
        max_length= 100
        )
    material = models.CharField(
        verbose_name='Металл',
        max_length=100
        )
    def __str__(self):
        return self.vid
    is_original = models.BooleanField(default = False)
    nation = models.ForeignKey('Nation', on_delete=models.CASCADE)
    avtomob = models.ForeignKey('Avtomob', on_delete=models.CASCADE)

