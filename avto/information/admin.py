from django.contrib import admin
from .models import Avtomob, Nation, Zapchasti

# Register your models here.
admin.site.register(Avtomob)
admin.site.register(Nation)
admin.site.register(Zapchasti)