from rest_framework import serializers
from .models import *


class AvtomobDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model=Avtomob
        fields='__all__'

class NationDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model=Nation
        fields='__all__'

class ZapchastiDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Zapchasti
        fields = '__all__'

class AvtomobListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Avtomob
        fields = '__all__'

class NationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Nation
        fields = '__all__'

class ZapchastiListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Zapchasti
        fields = '__all__'
